# vue_netlify_ci
![Header Image](https://cdn-images-1.medium.com/max/2400/1*DbMDy3SzlgdX-7jItNPggg.png)

> Reference repository on deploying a Vue.js app to Netlify using GitLab's CI/CD pipeline.

Netlify Build
[![Netlify Status](https://api.netlify.com/api/v1/badges/d8faacb1-77fe-4507-8470-627744fe9301/deploy-status)](https://app.netlify.com/sites/vue-netlify-ci/deploys)

GitLab CI Build
![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/mstrlaw/vue_netlify_ci.svg)

Full article explaining each step can be found on [Medium](https://medium.com/@mstrlaw/deploying-vue-js-to-netlify-using-gitlab-continuous-integration-pipeline-1529a2bbf170).


#### Project setup
```
npm install
```

#### Compiles and hot-reloads for development
```
npm run serve
```

#### Run your unit tests
```
npm run test:unit
```

Feedback is appreciated.
